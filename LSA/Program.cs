﻿using System;
using System.Collections.Generic;

namespace LSA
{
    static public class WordCleaner
    {
        static private Stoplist DK_stop = new Stoplist(@"C:\Users\johanvts\Documents\Visual Studio 2013\Projects\lsa-dansk\LSA\stop.txt");
        static public List<string> Clean(string input)
        {
            string sentence = input.ToLower();
            string[] words = sentence.Split(' ');
            List<string> result = new List<string>();
            foreach(string word in words)
            {
                string normalizedWord = Utilities.RemoveSpecialCharacters(word);
                if (normalizedWord.Length > 0)
                {
                    if (!DK_stop.IsStopword(normalizedWord))
                        result.Add(Stemmer.Stem(normalizedWord));    
                }
            }
            if (result.Count == 0)
                result.Add("");
            return result;
        }

        
    }
}
