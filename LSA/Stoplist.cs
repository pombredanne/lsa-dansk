﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LSA
{
    class Stoplist
    {
        private HashSet<String> _stoplist = new HashSet<string>();
        public HashSet<String> StopList
        {
            get { return _stoplist; }
            private set { _stoplist = value; }
        } 

        public Stoplist(String filename)
        {
            FileInfo file = new FileInfo(filename);
            StreamReader reader = file.OpenText();
            string line;
            while ((line = reader.ReadLine()) != null)
            {
                string raw_stopword = line.Split('|').First();
                string stopword = raw_stopword.Trim();
                this._stoplist.Add(stopword);
            }
        }

        public bool IsStopword(string word)
        {
            return _stoplist.Contains(word);
        }
    }
}
