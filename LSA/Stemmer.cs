﻿using System;
using System.Linq;

namespace LSA
{
    static class Stemmer
    {
        static public bool IsVowel(char letter)
        {
            char[] vowels = { 'a', 'e', 'i', 'o', 'u', 'y', 'æ', 'å', 'ø' };
            return vowels.Contains(letter);
        }

        static public string R1(string word)
        {
            string R1 = "";
            char[] charsRead = word.ToCharArray();
            int wordindex = charsRead.Length-1;
            for (int i = 1; i <=wordindex; i++) //Start from 1 to garantee region before R1 has size min 3.
            {
                char c = charsRead[i];
                if (IsVowel(c) && i < wordindex)
                {
                    if (!IsVowel(charsRead[i + 1]))
                    {
                        R1 = new string(charsRead, i + 2, charsRead.Length - (i+2));
                        break;
                    }
                }

            }
            return R1;
        }

        static public bool IsValidEnding(char letter)
        {
            char[] endings =
                {
                    'a', 'b', 'c', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 't', 'v', 'y',
                    'z', 'å'
                };
            return endings.Contains(letter);
        }

        public static string DeleteLongestSuffix(string word,string r1)
        {
            string removed = word;
            string[] suffixes =
                {
                    "hed", "ethed", "ered", "e", "erede", "ende", "erende", "ene", "erne",
                    "ere", "en", "heden", "eren", "er", "heder", "erer", "heds", "es",
                    "endes", "erendes", "enes", "ernes", "eres", "ens", "hedens",
                    "erens", "ers", "ets", "erets", "et", "eret", "s"
                };
            String longest = (from s in suffixes orderby s.Length descending where r1.EndsWith(s) select s).FirstOrDefault();
            if (longest == "s")
            {
                if (IsValidEnding(word[(word.Length - 2)]))
                {
                    removed=word.Remove(word.Length - 1);
                }
            }
            else if (longest!=null)
            {
                removed=word.Remove(word.Length - longest.Length);
            }
            return removed;
        }

        public static string CleanKt(string cleanSuffix)
        {
            string[] kts = {"gd", "gt", "kt", "dt"};
            if (kts.Any(kt => cleanSuffix.EndsWith(kt)))
            {
                return cleanSuffix.Remove(cleanSuffix.Length - 1);
            }
            return cleanSuffix;
        }

        public static string DeleteSuffixPart2(string ktClean)
        {
            string noSuf = ktClean;
            if (ktClean.EndsWith("igst"))
            {
                noSuf=ktClean.Remove(ktClean.Length - 2);
            }
            string removed = noSuf;
            string[] suffixes =
                {
                    "ig","lig","elig","els","løst"
                };
            String longest = (from s in suffixes orderby s.Length descending where noSuf.EndsWith(s) select s).FirstOrDefault();
            if (longest == "løst")
            {
                removed = CleanKt(noSuf.Remove(noSuf.Length - 1));
            }
            else if (longest != null)
            {
                removed = noSuf.Remove(noSuf.Length - longest.Length);
            }
            return removed;
        }

        static public string Undouble(string ending)
        {
            if (ending.Length>1 && !IsVowel(ending[ending.Length - 1]))
            {
                if (ending[ending.Length - 2] == ending[ending.Length - 1])
                {
                    return ending.Remove(ending.Length - 1);
                }
            }
            return ending;
        }

        static public string Stem(string word)
        {
            string nosuffix = DeleteLongestSuffix(word, R1(word));
            string cleankt = CleanKt(nosuffix);
            string deleteSuffixpt2 = DeleteSuffixPart2(cleankt);
            string undouble = Undouble(deleteSuffixpt2);
            return undouble;
        }
    }
}
